package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type Article struct {
	Title   string `json:"Title"`
	Desc    string `json:"desc"`
	Content string `json:"content"`
}

// let's declare a global Articles array
// that we can then populate in our main function
// to simulate a database
var articles []Article
var article Article

func index(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "index page")
	log.Println(&article)
}

func main() {
	http.HandleFunc("/", index)

	http.HandleFunc("/books", func(w http.ResponseWriter, req *http.Request) {
		articles = []Article{{Title: "def", Desc: "de", Content: "edf"}, Article{Title: "def", Desc: "de", Content: "edf"}}
		json.NewEncoder(w).Encode(articles)

	})
	http.HandleFunc("/books/create", PostArticle)
	http.ListenAndServe(":8787", nil)
}

func PostArticle(w http.ResponseWriter, req *http.Request) {
	var article Article
	if req.Method == "POST" {
		json.NewDecoder(req.Body).Decode(&article)
		log.Println((article.Content))
	}
}
